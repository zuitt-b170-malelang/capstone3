import './App.css';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavBar';
import React from 'react';
import Home from './pages/Home';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Register from './pages/Register'

export default function App() {
  return (
    <Router>
      <AppNavbar/>
       <Container>
         {/* <Routes>
         <Route path="/" element={<Home/>}/>
         
         </Routes> */}
         {/* <Home/> */}
         <Register/>
       </Container>

    </Router>
  )
}