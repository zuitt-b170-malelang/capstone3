import Banner from '../components/Banner';

export default function Home(){

	const data = {
		title: "Kape Dayon",
		content: "A place for coffee, friends, and fun!",
		destination: "/products",
		label: "Explore"
	}

	return(
		<>
			<Banner bannerProp={data}/>
		</>
	)
}